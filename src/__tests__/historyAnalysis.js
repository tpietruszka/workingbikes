import {DateTime} from 'luxon'
const path = require('path')
import { loadCompressedJson } from '../jsonLoading.js'
import {
  parkedPeriodQuantile,
  parkedPeriodThreshold,
  referenceBikesForPlace,
  timeSinceLastLongTrip,
} from '../historyAnalysis.js'
import { greatCircleDistance } from '../utils.js'
import StationsMap from '../StationsMap.js'

var cities, places, histories, distanceFunc; // loaded in beforeAll
const one_hour = 1000 * 60 * 60;


beforeAll(() => {
  let l1 = loadCompressedJson(path.resolve(__dirname, 'fixtures', 'nextbike-live.json.gz')).then(data => {
    cities = StationsMap.prototype._getAllCities(data);
    places = StationsMap.prototype._getAllPlaces(cities);
    distanceFunc = (num1, num2) => {
      const p1 = places[num1];
      const p2 = places[num2];
      return greatCircleDistance(p1.lat, p1.lng, p2.lat, p2.lng)
    }
  });
  let l2 = loadCompressedJson(path.resolve(__dirname, 'fixtures', 'histories.json.gz')).then(data => {
    histories = data;
  });
  return Promise.all([l1, l2]);
});

const hist_small = {
  '99991': [
    ['2019-10-22T16:51:37Z', 23845068],
    ['2019-10-21T12:15:10Z', -1],
    ['2019-10-17T09:15:16Z', 5108627],
    ['2019-10-17T08:43:12Z', -1],
    ['2019-10-17T08:42:11Z', 4660971]
  ],
  '99992': [
    ['2019-10-22T16:30:00Z', 23845068],
    ['2019-10-21T12:15:10Z', -1],
    ['2019-10-17T09:15:16Z', 5108627],
    ['2019-10-17T08:43:12Z', -1],
    ['2019-10-17T08:42:11Z', 4660971]
  ],
  '99958': [
    ['2019-10-11T02:01:14Z', -1]
  ],
  '99943': [
    ['2019-10-18T12:25:52Z', -1],
    ['2019-10-17T13:15:02Z', 5108627],
    ['2019-10-17T12:59:27Z', -1]
  ],
  '99942': [
    ['2019-10-20T13:21:51Z', 5108627],
    ['2019-10-20T13:07:19Z', -1],
    ['2019-10-17T16:47:30Z', 5108627],
    ['2019-10-17T16:44:21Z', -1],
    ['2019-10-17T16:42:18Z', 5108627]
  ],
  '99941': [
    ['2019-10-20T09:53:27Z', 23715437],
    ['2019-10-20T09:49:20Z', -1],
    ['2019-10-17T16:43:19Z', 5108627],
    ['2019-10-17T15:11:19Z', -1]
  ],
  '100': [
    ['2019-10-20T09:53:27Z', 23715437],
    ['2019-10-20T09:49:20Z', -1],
    ['2019-10-17T16:43:19Z', 23715437],
    ['2019-10-17T15:11:19Z', -1]
  ],
};

describe('Duration thresholds calculations', () => {
  it('returns truthy thresholds for an example place', () => {
    const warsaw_main_city = 210;
    const city = cities[warsaw_main_city];
    const result = parkedPeriodThreshold(city.places[0], cities, histories);
    expect(result.t_low).toBeTruthy();
    expect(result.t_high).toBeTruthy();
  });

  it('returns a default value for an empty city', () => {
    const place = {city_uid: 3, bike_list: []};
    const cities = {3: {uid: 3, related_cities: [3], places: [place]}};
    const result = parkedPeriodThreshold(place, cities, hist_small);
    expect(result.t_low).toBe(3 * 6 * one_hour);
    expect(result.t_high).toBe(3 * 3 * 6 * one_hour);
  });

  it('returns a min value for a city with one recently parked bike', () => {
    const place = {city_uid: 3, bike_list: [{number: '99992'}]};
    const cities = {3: {uid: 3, related_cities: [3], places: [place]}};
    const result = parkedPeriodThreshold(place, cities, hist_small);
    expect(result.t_low).toBe(3 * one_hour);
    expect(result.t_high).toBe(9 * one_hour);
  });

  it('returns a valid value for a simple non-default non-minimal case', () => {
    const place = {city_uid: 3, bike_list: [
      {number: '99942'},
      {number: '99941'},
      {number: '99991'},
    ]};
    const cities = {3: {uid: 3, related_cities: [3], places: [place]}};
    const result = parkedPeriodThreshold(place, cities, hist_small);
    let middle_time = Date.parse(hist_small['99991'][0][0]) - Date.parse(hist_small['99942'][0][0]);
    expect(result.t_low).toBe(3 * middle_time); // 2 * median
    expect(result.t_high).toBe(9 * middle_time);
  });
  // // TAKES >20s
  // it('returns truthy thresholds for example places from all cities', () => {
  //   for (const city of Object.values(cities)) {
  //     const place = city.places[0];
  //     const result = parkedPeriodThreshold(place, cities, histories);
  //     console.log(city.name, result.t_low / 1000. / 60 / 60);
  //     expect(result.t_low).toBeTruthy();
  //     expect(result.t_high).toBeTruthy();
  //   }
  // });
});

describe('Quantile calculations', () => {
  it('correctly calculates quantiles for a sample country', () => {
    // warsaw is a country with 3 cities: warsaw, warsaw-stacje partnerskie, warsaw-orlen
    const warsaw_main_city = 210;
    const city = cities[warsaw_main_city];
    const place_ex = city.places[0];
    const ref_bikes = referenceBikesForPlace(place_ex, cities);
    expect(parkedPeriodQuantile(ref_bikes, histories, 0)).toBe(0);
    expect(parkedPeriodQuantile(ref_bikes, histories, 1)).toBe(1194150000);
    expect(parkedPeriodQuantile(ref_bikes, histories, 0.5)).toBe(38331000);
    expect(parkedPeriodQuantile(ref_bikes, histories, 0.75)).toBe(107608000);
    expect(parkedPeriodQuantile(ref_bikes, histories, 0.25)).toBe(8998000);
  });
  it('returns NaN if there are no parked bikes', () => {
    expect(parkedPeriodQuantile([], hist_small, 0.5)).toBe(NaN);
    let only_moving_bikes = ['99958', '99840'];
    expect(parkedPeriodQuantile(only_moving_bikes, hist_small, 0.5)).toBe(NaN);
  });

  it('works correctly for few-bike cases', () => {
    let most_recently_parked = 99991; // the most recent timestamp in the data
    expect(parkedPeriodQuantile([most_recently_parked], hist_small, 0.5)).toBe(0);
    expect(parkedPeriodQuantile([most_recently_parked], hist_small, 1)).toBe(0);
    expect(parkedPeriodQuantile([most_recently_parked], hist_small, 0)).toBe(0);

    let three_parked = ['99942', '99941', '99991'];
    let middle_time = Date.parse(hist_small['99991'][0][0]) - Date.parse(hist_small['99942'][0][0]);
    expect(parkedPeriodQuantile(three_parked, hist_small, 0.5)).toBe(middle_time);

    let one_normal_one_no_hist = ['99942', 'asdqwe'];
    let parked_period = Date.parse(hist_small['99991'][0][0]) - Date.parse(hist_small['99942'][0][0]);
    expect(parkedPeriodQuantile(one_normal_one_no_hist, hist_small, 0.5)).toBe(parked_period);
    expect(parkedPeriodQuantile(one_normal_one_no_hist, hist_small, 0)).toBe(parked_period);
    expect(parkedPeriodQuantile(one_normal_one_no_hist, hist_small, 1)).toBe(parked_period);
  });
});

describe('timeSinceLastLongTrip', () => {
  it('Returns the length of last parked period in a typical case', () => {
    const bike_hist = hist_small['99991'];
    const currentTime = DateTime.fromISO('2019-10-22T17:00:00Z')
    const res = timeSinceLastLongTrip(bike_hist, currentTime, distanceFunc);
    const correct = currentTime - DateTime.fromISO(bike_hist[0][0]);
    expect(res).toBe(correct);
  });

  it('Ignores short trips', () => {
    const bike_hist = hist_small['100'];
    const currentTime = DateTime.fromISO('2019-10-22T17:00:00Z')
    const res = timeSinceLastLongTrip(bike_hist, currentTime, distanceFunc);
    const correct = currentTime - DateTime.fromISO(bike_hist[2][0]);
    expect(res).toBe(correct);
  });
});
