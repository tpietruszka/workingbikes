import React, { Component } from 'react';
import './App.css';
import StationsMap from './StationsMap.js'

class App extends Component {
  render() {
    return (
      <div className="App">
        <StationsMap />
      </div>
    );
  }
}

export default App;
