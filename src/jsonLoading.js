const fs = require('fs');
const zlib = require('zlib');

function streamToString(stream) {
  const chunks = []
  return new Promise((resolve, reject) => {
    stream.on('data', chunk => chunks.push(chunk))
    stream.on('error', reject)
    stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')))
  })
}

export function loadCompressedJson(path) {
  // loads a gzipped json file, returns a promise
  const fileContents = fs.createReadStream(path);
  const unzip = zlib.createGunzip();
  const unzippedStream = fileContents.pipe(unzip);
  return streamToString(unzippedStream).then(content => JSON.parse(content));
}
